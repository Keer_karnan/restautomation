package com.example.demo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/restservice")
@CrossOrigin
public class MyRestController {

	@RequestMapping(value="/get")
	public String getmethod() {
		return "Rest API hit(method=get)";
	}

	@RequestMapping(value="/post")
	public String postmethod() {
		return "Rest API hit(method=post)";
	}	
}
