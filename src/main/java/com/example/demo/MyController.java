package com.example.demo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dao.MyRestCallInterface;
import com.example.demo.dao.Requestdata;

@Controller
@CrossOrigin
public class MyController {
	@Autowired
	MyRestCallInterface r;
	RestTemplate resttemplate = new RestTemplate();
	
	@RequestMapping(value = "/errorpage")
	public String errorpage() {
	        return "errorpage";
	    }
	
	@Autowired
	Requestdata rd;
	
	@RequestMapping(method=RequestMethod.GET,
					value="/get",
					headers="Accept=application/json, application/xml, text/plain")
	public String send_url_using_rest_template_and_get_response(HttpServletRequest req, Model model) {
		String url = req.getParameter("endpoint");
		String input = req.getParameter("userinput");
		String expected_response = req.getParameter("inp2");
		if (rd.checkIfURLExists(url)) {
			System.out.println("URL Exists");
		} else {
			   return errorpage();
		}
		String color = "";
		if(expected_response == null)
			color = "yellow";
		else {
			boolean state = r.getCall(url,input).getBody().equals(expected_response);
			if(state == true) {
				color = "Responses Match";
			}
			else color = "Responses do not Match";
		}
        model.addAttribute("response", rd.allRequests());
        System.out.println(r.getCall(url,input).getBody());
        double time1 = rd.average_time();
        model.addAttribute("time", time1);
        model.addAttribute("color", color);
        return "ResultFile";
     }
	
	@RequestMapping(method=RequestMethod.POST,
					value="/post",
					headers="Accept=application/json, application/xml, text/plain")
	public String post_using_rest_template(HttpServletRequest req, Model model) {
		String url = req.getParameter("endpoint");
		String input = req.getParameter("userinput");
		String expected_response = req.getParameter("inp2");
		if (rd.checkIfURLExists(url)) {
			System.out.println("URL Exists");
		} else {
			   return errorpage();
		}
		String color = "";
		if(expected_response == null)
			color = "yellow";
		else {
			boolean state = r.getCall(url,input).getBody().equals(expected_response);
			if(state == true) {
				color = "Responses Match";
			}
			else color = "Responses do not Match";
		}
		r.postCall(url,input);
        model.addAttribute("response", rd.allRequests());
		double time1 = rd.average_time();
        model.addAttribute("time", time1);
        model.addAttribute("color", color);
		return "ResultFile";
			
	}
}
