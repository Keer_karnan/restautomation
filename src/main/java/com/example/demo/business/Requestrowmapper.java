package com.example.demo.business;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.demo.business.Requests;

public class Requestrowmapper implements RowMapper<Requests> {

	@Override
	public Requests mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Requests request = new Requests();
		request.setInput(rs.getString("Request"));
		request.setOutput(rs.getString("Response"));
		request.setTime(rs.getInt("Timetaken_ms"));
		return request;
	}
}
