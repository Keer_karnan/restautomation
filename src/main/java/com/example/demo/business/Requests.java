package com.example.demo.business;

public class Requests {
	private String input;
	private String output;
	private int time;
	public String getInput() {
		return input;
	}
	public void setInput(String input) {
		this.input = input;
	}
	public String getOutput() {
		return output;
	}
	public void setOutput(String output) {
		this.output = output;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public Requests() {
		super();
	}
	public Requests(String input, String output, int time) {
		super();
		this.input = input;
		this.output = output;
		this.time = time;
	}
	
}
