package com.example.demo.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.example.demo.dao.Requestdata;

@Service
public class RequestRepository {
	private Requestdata dao;
	
	@Autowired
	public RequestRepository(@Qualifier("jdbctemplate")Requestdata request) {
		this.dao = request;
	}
	
	public List<Requests> allRequests(){
		return dao.allRequests();
	}

	public int deleteRequest() {
		return dao.deleteRequest();
	}
	
	public int addRequest(Requests s) {
		return dao.addRequest(s);
	}
	
	public double average_time() {
		return dao.average_time();
	}
	
}