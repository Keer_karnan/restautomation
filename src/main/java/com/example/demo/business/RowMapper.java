package com.example.demo.business;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface RowMapper<T> {
	public Requests mapRow(ResultSet rs, int rowNum) throws SQLException;
}
