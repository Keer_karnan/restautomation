package com.example.demo.dao;

import org.springframework.http.ResponseEntity;

public interface MyRestCallInterface {
	public ResponseEntity<String> getCall(String url, String input);
	public ResponseEntity<String> postCall(String url, String input);
}
