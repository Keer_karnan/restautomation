package com.example.demo.dao;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.example.demo.business.Requestrowmapper;
import com.example.demo.business.Requests;
import com.google.gson.Gson;


@Component
@Qualifier("jdbctemplate")
public class MySQLJDBC implements Requestdata {
	private Logger logger =
		      LoggerFactory.getLogger(MySQLJDBC.class);
	
	@Autowired
	JdbcTemplate template;

	@Override
	public List<Requests> allRequests() {
	    // TODO Auto-generated method stub
	    List<Requests> requests = template.query(
	            "SELECT * From Requests", 
	            new Requestrowmapper());
	    return requests;
	}

	@Override
	public int addRequest(Requests s) {
		// TODO Auto-generated method stub
			Gson g = new Gson();
			Object javaobj = g.fromJson(s.getInput(), Object.class);
			return template.update("INSERT INTO Requests(Request, Response, Timetaken_ms, req_json) VALUES(?,?,?,?)", 
					s.getInput(), s.getOutput(),s.getTime(),g.toJson(javaobj));
	}

	@Override
	public int deleteRequest() {
		// TODO Auto-generated method stub
		return template.update("DELETE from Requests ");
	}
	
	@Override
	public double  average_time() {
		// TODO Auto-generated method stub
		return template.queryForObject("SELECT AVG(Timetaken_ms) FROM Requests", Double.class);
	}
	
	public boolean checkIfURLExists(String targetUrl) {
	        HttpURLConnection httpUrlConn;
	        try {
	            httpUrlConn = (HttpURLConnection) new URL(targetUrl)
	                    .openConnection();
	            httpUrlConn.setRequestMethod("HEAD");
	            httpUrlConn.setConnectTimeout(30000);
	            httpUrlConn.setReadTimeout(30000);
	            System.out.println("Response Code: "
	                    + httpUrlConn.getResponseCode());
	            System.out.println("Response Message: "
	                    + httpUrlConn.getResponseMessage());
	 
	            return (httpUrlConn.getResponseCode() == HttpURLConnection.HTTP_OK);
	        } catch (Exception e) {
	            System.out.println("Error: " + e.getMessage());
	            logger.error("Error in the URL method details: " +e.getMessage());
	            return false;
	        }
	}
}
