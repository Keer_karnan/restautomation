package com.example.demo.dao;

import java.util.List;

import com.example.demo.business.Requests;

public interface Requestdata {
	public List<Requests> allRequests();
	public int addRequest(Requests s);
	public int deleteRequest();
	public double average_time();
	boolean checkIfURLExists(String url);
	
}
