package com.example.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.demo.business.Requests;
import com.google.gson.Gson;

@Component
public class MyRestCalls implements MyRestCallInterface {
	@Autowired
	Requestdata rd;
	
	RestTemplate resttemplate;
	@Override
	public ResponseEntity<String> getCall(String url, String input) {
		resttemplate = new RestTemplate();
		long start =System.nanoTime();
		ResponseEntity<String> response = resttemplate.getForEntity(url, String.class);
		long end =System.nanoTime();
		int time = (int) ((end-start)/1000000);
        rd.addRequest(new Requests(input, response.getBody(), time));
		return response;
	}
	
	@Override
	public ResponseEntity<String> postCall(String url, String input) {
		resttemplate = new RestTemplate();		
		long start =System.nanoTime();
		ResponseEntity<String> response = resttemplate.postForEntity(url, input,String.class);
		long end =System.nanoTime();
		Gson g = new Gson();
		Object javaobj = g.fromJson(input, Object.class);
		String str = g.toJson(javaobj);
		rd.addRequest(new Requests(input,str,(int)((end-start)/1000000)));
		return response;
	}
}
