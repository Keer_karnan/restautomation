package com.example.demo.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.demo.Application;

public class Logdata {

	private Logger logger =
      LoggerFactory.getLogger(Application.class);

	public void logExample() {
		  logger.warn("Log link clicked");
	}
}