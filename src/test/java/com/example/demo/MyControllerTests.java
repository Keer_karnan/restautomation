package com.example.demo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MyControllerTests {
	@Autowired
    MockMvc mockMvc;
	
	@Test
    public void testGetHomePage() {
        try {
			mockMvc.perform(get("/"))
			       .andExpect(status().isOk())
			       .andExpect(xpath("//h1[contains(text(), 'Rest')]").exists());
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	@Test
    public void testGetFirstPage() {
        try {
			mockMvc.perform(get("/firstpage.html"))
			       .andExpect(status().isOk())
			       .andExpect(xpath("//*[contains(text(), 'REST Automation')]").exists());
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	@Test
    public void testGetResponsePage() {
        try {
			mockMvc.perform(get("/responsepage.html"))
			       .andExpect(status().isOk())
			       .andExpect(xpath("//*[contains(text(), 'Request Logs')]").exists());
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}