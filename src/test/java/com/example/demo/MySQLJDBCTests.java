package com.example.demo;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.Application;
import com.example.demo.business.Requests;
import com.example.demo.dao.Requestdata;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class MySQLJDBCTests {
	
	@Autowired
	Requestdata rd;
		
	@Test
	public void test_addRequest_whether_data_gets_added_or_not() {
		int x = rd.addRequest(new Requests("L","l",9));
		assertEquals(1,x);
	}
	
	@Test
	public void test_deleteRequests() {
		Requests s = new Requests();
		rd.deleteRequest();
		assertEquals(null,s.getInput());
	}
	
	@Test
	public void test_getRequests() {
		rd.deleteRequest();
		List<Requests> s = rd.allRequests();
		assertEquals(true,s.isEmpty());
		rd.addRequest(new Requests("L","l",9));
		s = rd.allRequests();
		assertEquals(false,s.isEmpty());
		rd.deleteRequest();
	}
}
